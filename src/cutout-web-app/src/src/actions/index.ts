import Cookies from "universal-cookie";
import config from "../app.config";

type Dispatch = (action: any) => null;
const cookies = new Cookies();

export function fetchAuthToken() {
	return async (dispatch: Dispatch) => {
		const endpoint = config.tokenUrl;
		let tokenRequest
		let tokenResponse
		let secureCookie = false;
		if (['', 'localhost', '127.0.0.1'].includes(config.hostname)) {
			// console.log(`config.hostname: ${config.hostname}`);
			tokenResponse = {
				"token": "dummyToken",
				"maxAge": "3600",
				'userInfo': config.testUserInfo,
			}
			tokenRequest = {
				status: 200,
			}
		} else {
			tokenRequest = await fetch(endpoint, {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				}
			});
			secureCookie = true;
			if (tokenRequest.status >= 200 && tokenRequest.status <= 299) {
				tokenResponse = await tokenRequest.json();
			}
		}
		let loginResponseText = "";
		if (tokenRequest !== undefined && tokenRequest.status >= 400 && tokenRequest.status <= 499) {
			window.location.assign(`${config.loginUrl}?next=${config.baseUrl}`);
		} else if (tokenRequest !== undefined && tokenRequest.status >= 200 && tokenRequest.status <= 299 && tokenResponse["token"] !== undefined ) {
			const cookieOptions = { maxAge: tokenResponse["maxAge"], secure: secureCookie };
			cookies.set("Authorization", `bearer ${tokenResponse["token"]}`, cookieOptions);
			cookies.set("UserInfo", tokenResponse["userInfo"], cookieOptions);
			return dispatch({
				type: 'login/succeeded',
				Authorization: `bearer ${tokenResponse["token"]}`,
				userInfo: tokenResponse["userInfo"],
			});
		} else {
			if (tokenRequest !== undefined && tokenRequest.statusText !== undefined) {
				loginResponseText = tokenRequest.statusText;
			}
			return dispatch({
				type: 'login/failed',
				loginResponseText: loginResponseText,
			});
		}
	};
}

export function logout() {
	return (dispatch: Dispatch) => {
		cookies.remove("Authorization");
		cookies.remove("UserInfo");
		return dispatch({
			type: 'logout'
		});
	};
}

export function displayError(message: string) {
	return (dispatch: Dispatch) => {
		return dispatch({
			type: 'errorMessage/display',
			message: message,
		});
	};
}


export function clearError() {
	return (dispatch: Dispatch) => {
		return dispatch({
			type: 'errorMessage/clear',
		});
	};
}

export function displayMessage(message: string) {
	return (dispatch: Dispatch) => {
		return dispatch({
			type: 'infoMessage/display',
			message: message,
		});
	};
}


export function clearMessage() {
	return (dispatch: Dispatch) => {
		return dispatch({
			type: 'infoMessage/clear',
		});
	};
}
