import { React, Component } from "react";
import Box from '@mui/material/Box';
import FormLabel from '@mui/material/FormLabel';
import FormControl from '@mui/material/FormControl';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import FormHelperText from '@mui/material/FormHelperText';

class SelectBand extends Component {

	constructor(props) {
		super(props);
		this.state = {
			ghz90: props.bands.includes('90GHz'),
			ghz150: props.bands.includes('150GHz'),
			ghz220: props.bands.includes('220GHz'),
		};
		this.state = {
			...this.state,
			inValidSelection: !this.validate(this.state),
		};
		this.handleChange = this.handleChange.bind(this);
		this.validate = this.validate.bind(this);
	}

	handleChange = (event) => {
		this.setState((state, props) => {
			props.setBands({
				...state,
				[event.target.name]: event.target.checked,
			});
			return {
				...state,
				[event.target.name]: event.target.checked,
			};
		}, () => {
			this.setState((state) => {
				return {
					...state,
					inValidSelection: !this.validate(state),
				};
			});
		});
	};

	validate = (state) => {
		return state.ghz90 || state.ghz150 || state.ghz220;
	};

	render() {
		return (
			<Box sx={{ display: 'flex' }}>
				<FormControl sx={{ m: 3 }} component="fieldset" variant="standard"
					error={this.state.inValidSelection}>
					<FormLabel component="legend">Select frequency bands</FormLabel>
					<FormGroup>
						<FormControlLabel
							control={
								<Checkbox checked={this.state.ghz90} onChange={this.handleChange} name="ghz90" />
							}
							label="90GHz"
						/>
						<FormControlLabel
							control={
								<Checkbox checked={this.state.ghz150} onChange={this.handleChange} name="ghz150" />
							}
							label="150GHz"
						/>
						<FormControlLabel
							control={
								<Checkbox checked={this.state.ghz220} onChange={this.handleChange} name="ghz220" />
							}
							label="220GHz"
						/>
					</FormGroup>
					<FormHelperText>Select at least one frequency band.</FormHelperText>
				</FormControl>
			</Box>
		);
	}
};


export default (SelectBand);
