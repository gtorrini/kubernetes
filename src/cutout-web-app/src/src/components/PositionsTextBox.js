import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';

export default function PositionsTextBox(props) {
	const [, setValue] = React.useState(props.value);

	const handleChange = (event) => {
		setValue(event.target.value);
		props.setPositions(event.target.value)
	};

	return (
		<Box
			component="form"
			sx={{
				'& .MuiTextField-root': { m: 1, width: '80%', 'minWidth': '25ch' },
			}}
			noValidate
			autoComplete="off"
		>
			<div>
				<TextField
					id="outlined-multiline-static"
					label="Cutout Positions"
					multiline
					rows={12}
					onChange={handleChange}
					value={props.value}
				/>
			</div>
		</Box>
	);
}
