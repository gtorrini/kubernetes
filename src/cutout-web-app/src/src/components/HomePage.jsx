import { Component } from "react";
import {
	Container,
	Grid,
	Link,
} from '@mui/material';
import { withStyles } from '@mui/styles';
import config from "../app.config";
import Cookies from "universal-cookie";
import HelpFormDialog from './HelpFormDialog.jsx'
const cookies = new Cookies();

const styles = theme => ({
	root: {
		color: theme.palette.primary,
		position: "relative",
		display: "flex",
		alignItems: "center",
		[theme.breakpoints.up("sm")]: {
			minHeight: 400,
			maxHeight: 1300,
		},
	},
	container: {
		"&": {
			backgroundColor: "#ffffff",
			marginTop: theme.spacing(3),
			marginBottom: theme.spacing(10),
			display: "flex",
			flexDirection: "column",
			alignItems: "center",
		},
		// "&, &:before": {
		// 	backgroundImage: `url(${backgroundImage})`,
		// 	backgroundPosition: "center",
		// 	position: "absolute",
		// 	left: 0,
		// 	right: 0,
		// 	top: 0,
		// 	bottom: 0,
		// 	zIndex: -2,
		// 	backgroundRepeat: "no-repeat",
		// 	backgroundSize: "cover",
		// 	opacity: 0.15,
		// },
	},
	codeblock: {
		border: "solid lightgray 1px",
		padding: "1rem 1rem",
		backgroundColor: "#EEE",
		wordWrap: "anywhere",
		fontFamily: "monospace",
	},
	codeblock2: {
		border: "solid lightgray 1px",
		padding: "1rem 1rem",
		backgroundColor: "#EEE",
		fontFamily: "monospace",
		whiteSpace: "pre",
	},
});

class HomePage extends Component {

	constructor(props) {
		super(props);
		this.state = {
			helpFormDialogOpen: false,
			helpFormDialogMessage: '',
		};
		this.handleSendClick = this.handleSendClick.bind(this);
		this.closeHelpFormDialog = this.closeHelpFormDialog.bind(this);
		this.setHelpFormMessage = this.setHelpFormMessage.bind(this);
	}

	async handleSendClick() {
		this.closeHelpFormDialog()
		
		const endpoint = `https://${config.hostname}${config.sendHelpFormUrl}`;
		const payload = {
			message: this.state.helpFormDialogMessage,
		}
		let response = await fetch(endpoint, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": cookies.get("Authorization")
			},
			body: JSON.stringify(payload)
		});
		if (response.status >= 200 && response.status <= 299) {
			this.props.displayMessage(`Message sent!`);
		} else {
			console.log(JSON.stringify(response.statusText, null, 2));
			this.props.displayError(`Error sending message: ${response.statusText}: ${await response.text()}`);
		}
	};
	
	openHelpFormDialog() {
		this.setState({
			helpFormDialogOpen: true,
		})
	};
	
	
	closeHelpFormDialog() {
		this.setState({
			helpFormDialogOpen: false,
		})
	};
	
	setHelpFormMessage(text) {
		this.setState({
			helpFormDialogMessage: text,
		})
	};
	
	
	render() {
		const { classes } = this.props;
		let authToken = null;
		let authTokenHTML = <></>;
		try {
			authToken = cookies.get("Authorization").substring(7);
			authTokenHTML = (
				<div>
					<h3 class="m-3">API token</h3>
					<p>
						You may use the access token shown below for programmatic use:
					</p>
					<p className={classes.codeblock}>
						{authToken}
					</p>
					<p>
						The <code>curl</code> example below illustrates how to use the API token in an authenticated HTTP request:
					</p>
					<p className={classes.codeblock2}>
						curl -X GET --silent \ <br />
						--header "Authorization: bearer {authToken.slice(0, 4)}...{authToken.slice(-4)}" \ <br />
						https://{config.hostname}{config.tokenRefreshPath}<br />
					</p>
					<h3 class="m-3">Help</h3>
					<p>
						If you have questions please <Link style={{ textDecoration: "none", cursor: "pointer" }} onClick={() => { this.openHelpFormDialog(); }}> use the help form 📧</Link>.
					</p>
				</div>
			);
		} catch (e) {}

		return (
			<Container className={classes.container}>
				<Grid container spacing={0}>
					<Grid item xs={12}>
						<h2 class="m-3">
							Welcome to the SPT-3G Cutout Service
						</h2>
						<p>
							The cutout service allows users to launch asynchronous jobs to cutout subsets of the
							SPT-3G data based on RA/DEC coordinates and areal dimensions about those coordinates.
							The SPT-3G job API enables comprehensive programmatic job management, including the
							ability to create, monitor and download the results of submitted jobs.
						</p>
						<h3 class="m-3">Documentation</h3>
						<p>
							Use the links below to learn more:
						</p>
						<p style={{ fontSize: "1.2rem" }}>
							📄 <a target="_blank" rel="noreferrer" href="https://spt3g.ncsa.illinois.edu/docs/api">Explore the SPT-3G API documentation.</a>
						</p>
						<p style={{ fontSize: "1.2rem" }}>
							📖 <a target="_blank" rel="noreferrer" href="https://gitlab.com/spt3g/kubernetes/-/blob/07e11150cd0d3e74a1b335c1393d737851399103/scripts/cutout_service/API_example.py">
								View an example Python script demonstrating how to use the API.</a>
						</p>
						{authTokenHTML}
					</Grid>
				</Grid>
				<HelpFormDialog
					open={this.state.helpFormDialogOpen}
					handleSendClick={this.handleSendClick}
					closeHelpFormDialog={this.closeHelpFormDialog}
					setHelpFormMessage={this.setHelpFormMessage}
				/>
			</Container >
		);
	}

}

export default withStyles(styles)(HomePage);
