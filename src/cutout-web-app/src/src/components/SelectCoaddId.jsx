import { React, Component } from "react";
import Box from '@mui/material/Box';
import FormLabel from '@mui/material/FormLabel';
import FormControl from '@mui/material/FormControl';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import FormHelperText from '@mui/material/FormHelperText';

class SelectCoaddId extends Component {

	constructor(props) {
		super(props);
		this.state = {
			option1: props.coaddIds.includes('yearly_winter_2020'),
			option2: props.coaddIds.includes('yearly_rawmap_v1_2019_2020'),
			option3: props.coaddIds.includes('yearly_cleanbeammap_v1_2019_2020'),
		};
		this.state = {
			...this.state,
			inValidSelection: !this.validate(this.state),
		};
		this.handleChange = this.handleChange.bind(this);
		this.validate = this.validate.bind(this);
	}

	handleChange = (event) => {
		this.setState((state, props) => {
			props.setCoaddIds({
				...state,
				[event.target.name]: event.target.checked,
			});
			return {
				...state,
				[event.target.name]: event.target.checked,
			};
		}, () => {
			this.setState((state) => {
				return {
					...state,
					inValidSelection: !this.validate(state),
				};
			});
		});
	};

	validate = (state) => {
		return state.option1 || state.option2 || state.option3;
	};

	render() {
		return (
			<Box sx={{ display: 'flex' }}>
				<FormControl sx={{ m: 3 }} component="fieldset" variant="standard"
					error={this.state.inValidSelection}>
					<FormLabel component="legend">Select Coadd ID for coadds only</FormLabel>
					<FormGroup>
						<FormControlLabel
							control={
								<Checkbox checked={this.state.option1} onChange={this.handleChange} name="option1" />
							}
							label="winter_2020"
						/>
						<FormControlLabel
							control={
								<Checkbox checked={this.state.option2} onChange={this.handleChange} name="option2" />
							}
							label="rawmap_v1_2019_2020"
						/>
						<FormControlLabel
							control={
								<Checkbox checked={this.state.option3} onChange={this.handleChange} name="option3" />
							}
							label="cleanbeammap_v1_2019_2020"
						/>
					</FormGroup>
					<FormHelperText></FormHelperText>
				</FormControl>
			</Box>
		);
	}
};


export default (SelectCoaddId);
