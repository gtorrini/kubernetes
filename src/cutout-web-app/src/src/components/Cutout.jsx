import { React, Component } from "react";
import {
	Container,
	Button,
	Grid,
	Paper,
	Typography,
} from '@mui/material';
import { withStyles } from '@mui/styles';
import { styled } from '@mui/material/styles';
import BasicDatePicker from './BasicDatePicker'
import SelectBand from './SelectBand.jsx'
import SelectFiletype from './SelectFiletype.jsx'
import SelectCoaddId from './SelectCoaddId.jsx'
import PositionsTextBox from './PositionsTextBox'
import PositionsFileButton from './PositionsFileButton.jsx'
import config from "../app.config";
import Cookies from "universal-cookie";
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';

const cookies = new Cookies();

const Item = styled(Paper)(({ theme }) => ({
	...theme.typography.body2,
	padding: theme.spacing(1),
	// textAlign: 'center',
	color: theme.palette.text.secondary,
}));

const styles = theme => ({
	root: {
		color: theme.palette.primary,
		position: "relative",
		display: "flex",
		alignItems: "center",
		[theme.breakpoints.up("sm")]: {
			minHeight: 400,
			maxHeight: 1300,
		},
	},
	container: {
		marginTop: theme.spacing(3),
		marginBottom: theme.spacing(10),
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
	backdrop: {
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
		opacity: 0.7,
		zIndex: -1,
	},
	background: {
		backgroundColor: "#ffffff",
		backgroundPosition: "center",
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
		backgroundSize: "cover",
		zIndex: -2,
	},
	releaseChip: {
		minWidth: 200,
		padding: 25,
		margin: theme.spacing(2),
		color: "#ffffff",
		fontWeight: "bold",
	},
	h1: {
		fontWeight: "bold",
		color: theme.palette.primary
	},
	h5: {
		marginBottom: theme.spacing(3),
		marginTop: theme.spacing(3),
	},
	more: {
		textAlign: "center",
		marginTop: theme.spacing(2),
	},
	versionSection: {
		margin: theme.spacing(0.5)
	},
	versionLine: {
		margin: theme.spacing(1)
	},
	versioning: {
		margin: theme.spacing(0.5),
		fontSize: 14,
		// display: "inline",
		fontWeight: "bold"
	},
	sectionDark: {
		display: "flex",
		overflow: "hidden",
		backgroundColor: theme.palette.fourth.main,
	},
	sectionLight: {
		display: "flex",
		overflow: "hidden"
	},
	footer: {
		overflow: "hidden",
		backgroundColor: "#ffffff"
	},
	link: {
		color: theme.palette.secondary.main,
		textDecoration: "none",
		fontWeight: "bold"
	},
	sectionContainers: {
		marginTop: theme.spacing(15),
		marginBottom: theme.spacing(30),
		display: "flex",
		position: "relative",
	},
	item: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		padding: theme.spacing(0, 3),
	},
	image: {
		height: 100,
	},
	title: {
		marginTop: theme.spacing(3),
		minHeight: theme.spacing(10),
	},
	content: {
		marginTop: theme.spacing(3),
		marginBottom: theme.spacing(3),
		textAlign: "center",
	},
	footerContainer: {
		marginTop: theme.spacing(4),
		marginBottom: theme.spacing(4),
		display: "flex",
	},
	iconsWrapper: {
		height: 80,
	},
	icons: {
		display: "flex",
	},
	icon: {
		height: 80,
		borderRadius: 5,
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		marginRight: theme.spacing(1)
	},
	list: {
		margin: 0,
		listStyle: "none",
		paddingLeft: 0,
	},
	listItem: {
		paddingTop: theme.spacing(0.5),
		paddingBottom: theme.spacing(0.5),
	},
});

class Cutout extends Component {

	constructor(props) {
		super(props);

		/*
				In case we want to initialize the dates
				let startDate = new Date();
				let endDate = new Date(startDate.getTime());
				startDate.setDate(startDate.getDate() - 1);
		*/

		/*  Set default dates as min/max range of the data  */
		let startDate = new Date('01/01/2019');
		let endDate = new Date('11/30/2021');

		this.state = {
			showDates: false,
			startDate: startDate,
			endDate: endDate,
			bands: config.cutouts.defaultBands,
			filetypes: config.cutouts.defaultFiletypes,
			positions: config.cutouts.defaultPositions,
			coaddIds: config.cutouts.defaultCoaddIds,
			errorMessage: "",
			messageOpen: false,
			emailNotify: true,
			nofits: false,
			lightcurve: false,
			coverage: true,

		};
		this.handleDailies = this.handleDailies.bind(this);
		this.setStartDate = this.setStartDate.bind(this);
		this.setEndDate = this.setEndDate.bind(this);
		this.setBands = this.setBands.bind(this);
		this.setFiletypes = this.setFiletypes.bind(this);
		this.setCoaddIds = this.setCoaddIds.bind(this);
		this.setPositions = this.setPositions.bind(this);
		this.submitJob = this.submitJob.bind(this);
		this.handleResponse = this.handleResponse.bind(this);
		this.handleEmailSwitch = this.handleEmailSwitch.bind(this);
		this.handleLightcurve = this.handleLightcurve.bind(this);
		this.handleNofits = this.handleNofits.bind(this);
		this.handleCoverage = this.handleCoverage.bind(this);
	}

	handleDailies(event) {
		this.setState({
			showDates: event.target.checked,
		})
	}

	setStartDate(date) {
		this.setState({
			startDate: date
		})
	}

	setEndDate(date) {
		this.setState({
			endDate: date
		})
	}

	convertDateString(date) {
		if (date == null) {
			return date
		} else {
			return date.toISOString().split('T')[0]
		}
	}

	setPositions(textInput) {
		this.setState({
			positions: textInput
		})
	}

	handleEmailSwitch(event) {
		this.setState({
			emailNotify: event.target.checked,
		})
	}

	handleLightcurve(event) {
		this.setState({
			lightcurve: event.target.checked,
		})
	}

	handleNofits(event) {
		this.setState({
			nofits: event.target.checked,
		})
	}

	handleCoverage(event) {
		this.setState({
			coverage: event.target.checked,
		})
	}

	setBands(selectedBands) {
		let bands = []
		if (selectedBands["ghz90"]) {
			bands.push('90GHz');
		}
		if (selectedBands["ghz150"]) {
			bands.push('150GHz');
		}
		if (selectedBands["ghz220"]) {
			bands.push('220GHz');
		}
		this.setState({
			bands: bands
		})
	}

	setFiletypes(selectedFiletypes) {
		let filetypes = []
		if (selectedFiletypes["passthrough"]) {
			filetypes.push('passthrough');
		}
		if (selectedFiletypes["filtered"]) {
			filetypes.push('filtered');
		}
		this.setState({
			filetypes: filetypes
		})
	}

	setCoaddIds(selectedCoaddIds) {
		let coaddIds = []
		if (selectedCoaddIds["option1"]) {
			coaddIds.push('yearly_winter_2020');
		}
		if (selectedCoaddIds["option2"]) {
			coaddIds.push('yearly_rawmap_v1_2019_2020');
		}
		if (selectedCoaddIds["option3"]) {
			coaddIds.push('yearly_cleanbeammap_v1_2019_2020');
		}
		this.setState({
			coaddIds: coaddIds
		})
	}

	async submitJob() {
		const endpoint = `https://${config.hostname}${config.jobSubmitUrl}`;
		const payload = {
			positions: this.state.positions,
			bands: this.state.bands,
			filetypes: this.state.filetypes,
			yearly_coadd: this.state.coaddIds,
			get_lightcurve: this.state.lightcurve,
			nofits: this.state.nofits,
			get_uniform_coverage: this.state.coverage,
			email: this.state.emailNotify,
		};
		const showDates = this.state.showDates;
		if (showDates===false) {
			payload.date_start = this.convertDateString(null);
			payload.date_end = this.convertDateString(null);
		} else {
			payload.date_start = this.convertDateString(this.state.startDate);
			payload.date_end = this.convertDateString(this.state.endDate);
		}
		let jobRequest = await fetch(endpoint, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": cookies.get("Authorization")
			},
			body: JSON.stringify(payload)
		});
		this.handleResponse(jobRequest)
	}

	async handleResponse(response) {
		if (response.status >= 200 && response.status <= 299) {
			let jobInfo = await response.json();
			// console.log(JSON.stringify(jobInfo, null, 2));
			this.props.displayMessage(`Job submitted successfully! Job ID: ${[jobInfo['jobId']]}`);
		} else {
			console.log(JSON.stringify(response.statusText, null, 2));
			this.props.displayError(`${response.statusText}: ${await response.text()}`);
		}
	}


	render() {
		const { classes } = this.props;
		let contents = <></>;
		const showDates = this.state.showDates;
		if (this.props.Authorization !== "" && this.props.Authorization !== undefined) {
			contents = (
				<Grid container spacing={4}>
					<Grid item sm={12} md={4}>
						<h3>Single Observation Section</h3>
						<Item elevation={0}>
							<FormGroup>
								<FormControlLabel control={
									<Switch
										checked={this.state.showDates}
										onChange={this.handleDailies}
										inputProps={{ 'aria-label': 'Use Single Observation maps' }}
									/>} label="Use Single Observation maps" />
							</FormGroup>
						</Item>
						{showDates ? <>
							<Item elevation={0}>
								<p> Select Date range for Single Observation maps only</p>
								<p> Acceptable range: 2019-01-01 to 2021-11-30</p>
								<BasicDatePicker whichDate="Start Date" setDate={this.setStartDate} initDate={this.state.startDate} />
							</Item>
							<Item elevation={0}>
								<BasicDatePicker whichDate="End Date" setDate={this.setEndDate} initDate={this.state.endDate} />
							</Item> </>
							: <></>
						}
						<hr></hr>
						<h3>Filetype Section</h3>
						<Item elevation={0}>
							<SelectFiletype setFiletypes={this.setFiletypes} filetypes={this.state.filetypes} />
						</Item>
						<hr></hr>
						<h3>Coadd Section</h3>
						<Item elevation={0}>
							<SelectCoaddId setCoaddIds={this.setCoaddIds} coaddIds={this.state.coaddIds} />
						</Item>
					</Grid>
					<Grid item sm={12} md={8}>
						<Item elevation={0}>
							<SelectBand setBands={this.setBands} bands={this.state.bands} />
						</Item>
						<Item elevation={0}>
							<Typography variant="body1">Input a CSV-formatted table of cutout positions, (<code>RA,DEC</code>) in decimal degrees, (<code>XSIZE,YSIZE</code>) in arcmin. </Typography>
							<Typography variant="body1">Optional column with unique id/names can be added so columns are <code>OBJID, RA, DEC, XSIZE, YSIZE</code></Typography>
							<PositionsTextBox setPositions={this.setPositions} value={this.state.positions} />
							<PositionsFileButton setPositions={this.setPositions} value={this.state.positions} />
						</Item>
						<Item elevation={0}>
							<FormGroup>
								<FormControlLabel control={
									<Switch
										checked={this.state.lightcurve}
										onChange={this.handleLightcurve}
										inputProps={{ 'aria-label': 'Get lightcurves' }}
									/>} label="Get lightcurves for input positions" />
							</FormGroup>
						</Item>

						<Item elevation={0}>
							<FormGroup>
								<FormControlLabel control={
									<Switch
										checked={this.state.nofits}
										onChange={this.handleNofits}
										inputProps={{ 'aria-label': 'No thumbnails' }}
									/>} label="Do not make FITS thumbnails" />
							</FormGroup>
						</Item>

						<Item elevation={0}>
							<FormGroup>
								<FormControlLabel control={
									<Switch
										checked={this.state.coverage}
										onChange={this.handleCoverage}
										inputProps={{ 'aria-label': 'Select uniform coverage' }}
									/>} label="Select uniform coverage" />
							</FormGroup>
						</Item>
						<Item elevation={0}>
							<FormGroup>
								<FormControlLabel control={
									<Switch
										checked={this.state.emailNotify}
										onChange={this.handleEmailSwitch}
										inputProps={{ 'aria-label': 'Send email notification' }}
									/>} label="Send email notification" />
							</FormGroup>
						</Item>
						<Item elevation={0}>
							<Button variant="contained" onClick={this.submitJob}>Generate Cutouts</Button>
						</Item>
					</Grid>
				</Grid>
			);
		}
		return (
			<Container className={classes.container}>
				{contents}
			</Container>
		);
	}

}

export default withStyles(styles)(Cutout);
