type InfoMessageState = {
	display: Boolean,
	message: String,
}

type InfoMessageAction = {
	type: String,
	message: String,
}

const defaultState: InfoMessageState = {
	display: false,
	message: "",
};

const infoMessage = (state: InfoMessageState = defaultState, action: InfoMessageAction) => {
	switch (action.type) {
		case 'infoMessage/display':
			return Object.assign({}, state, { display: true, message: action.message });
		case 'infoMessage/clear':
			return Object.assign({}, state, { display: false, message: "" });
		default:
			return state;
	}
};

export default infoMessage;
