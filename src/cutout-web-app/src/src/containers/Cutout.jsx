import { connect } from "react-redux";
import CutoutComponent from "../components/Cutout";
import { displayError, displayMessage } from "../actions";


const mapStateToProps = (state) => {
	return {
		Authorization: state.user.Authorization,
		userInfo: state.user.userInfo,
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		displayError: (message) => {
			dispatch(displayError(message));
		},
		displayMessage: (message) => {
			dispatch(displayMessage(message));
		},
	};
};

const Cutout = connect(mapStateToProps, mapDispatchToProps)(CutoutComponent);

export default Cutout;
