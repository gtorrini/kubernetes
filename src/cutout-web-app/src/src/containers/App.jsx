import { connect } from "react-redux";
import AppComponent from "../components/App";
import { logout } from "../actions";
import { fetchAuthToken } from "../actions";
import { clearError, displayError, clearMessage, displayMessage } from "../actions";


const mapStateToProps = (state) => {
	return {
		Authorization: state.user.Authorization,
		userInfo: state.user.userInfo,
		loginError: state.user.loginError,
		loginResponseText: state.user.loginResponseText,
		errorMessageText: state.errorMessage.message,
		errorMessageDisplay: state.errorMessage.display,
		infoMessageText: state.infoMessage.message,
		infoMessageDisplay: state.infoMessage.display,
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		logout: () => {
			dispatch(logout());
		},
		fetchAuthToken: async () => {
			// console.log('Fetching auth token...')
			await dispatch(fetchAuthToken());
		},
		displayError: (message) => {
			dispatch(displayError(message));
		},
		clearError: () => {
			dispatch(clearError());
		},
		clearMessage: () => {
			dispatch(clearMessage());
		},
		displayMessage: (message) => {
			dispatch(displayMessage(message));
		},
	};
};

const App = connect(mapStateToProps, mapDispatchToProps)(AppComponent);

export default App;
