import {IndexRoute, Route} from "react-router";
import App from "./containers/App";
import Cutout from "./containers/Cutout";
import JobStatus from "./containers/JobStatus";
import HomePage from "./containers/HomePage";

export default (
	<Route path="/app" component={App}>
		<IndexRoute component={{homepage: HomePage}}/>
		<Route path="cutout" component={{cutout: Cutout}}/>
		<Route path="status" component={{status: JobStatus}}/>
	</Route>
);

