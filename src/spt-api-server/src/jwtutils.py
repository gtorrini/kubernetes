import jwt
from datetime import datetime, timedelta
from dbconnector import DbConnector
from global_vars import config

STATUS_OK = 'ok'
STATUS_ERROR = 'error'

# Get global instance of the job handler database interface
db = DbConnector(
    mysql_host=config['db']['host'],
    mysql_user=config['db']['user'],
    mysql_password=config['db']['pass'],
    mysql_database=config['db']['database'],
)


def encode_info(user_info={}, ttl=config['jwt']['ttlSeconds']):
    ## The original token from Keycloak has the key `sub` for the user ID.
    ## Subsequent `user_info` objects have `user_id` instead.
    if 'sub' in user_info:
        user_id = user_info['sub']
    else:
        user_id = user_info['user_id']
    encoded = jwt.encode({
        'user_id': user_id,
        'email': user_info['email'],
        'preferred_username': user_info['preferred_username'],
        'given_name': user_info['given_name'],
        'family_name': user_info['family_name'],
        'name': user_info['name'],
        'roles': user_info['roles'] if 'roles' in user_info else [],
        'groups': user_info['groups'] if 'groups' in user_info else [],
        'exp': datetime.utcnow() + timedelta(seconds=ttl)},
        config['jwt']['hs256Secret'],
        algorithm='HS256'
    )
    return encoded


def validate_token(token):
    response = {
        'status': STATUS_OK,
        'message': '',
        'http_code': 200,
        'decoded_token': None,
    }
    try:
        response["decoded_token"] = jwt.decode(token, config['jwt']['hs256Secret'], algorithms=["HS256"])
        denylist = db.get_denylist()
        if [user for user in denylist if response["decoded_token"]['user_id'] == user['user_id']]:
            response["status"] = STATUS_ERROR
            response["message"] = "User is denied."
            response["http_code"] = 403
    except jwt.InvalidSignatureError:
        response["status"] = STATUS_ERROR
        response["message"] = "Signature verification failed"
        response["http_code"] = 403
    except jwt.ExpiredSignatureError:
        response["status"] = STATUS_ERROR
        response["message"] = "Signature has expired"
        response["http_code"] = 403
    except jwt.DecodeError:
        response["status"] = STATUS_ERROR
        response["message"] = "Invalid header string"
        response["http_code"] = 400
    except Exception as e:
        response["status"] = STATUS_ERROR
        response["message"] = str(e).strip()
        response["http_code"] = 500
    return response


def refresh_token(token):
    response = {
        'status': STATUS_OK,
        'message': '',
        'token': '',
    }
    try:
        validate = validate_token(token)
        if validate["status"] != STATUS_OK:
            response["status"] = validate["status"]
            response["message"] = validate["message"]
            return response
        # If the token was valid, issue a new token to extend the session
        decoded_token = validate["decoded_token"]
        decoded_token.pop('exp', None)
        response['token'] = encode_info(user_info=decoded_token).decode('utf-8')
    except Exception as e:
        response["status"] = STATUS_ERROR
        response["message"] = f'''Error refreshing token: {e}'''
    return response


# The @authenticated decorator wraps RequestHandler subclasses and checks
# their Authorization header to validate Bearer tokens. It ends the request
# if there is not a valid token.
def authenticated(cls_handler):
    def wrap_execute(handler_execute):
        def check_auth(handler, kwargs):
            auth = handler.request.headers.get("Authorization")
            response = {
                'status': STATUS_OK,
                'message': '',
                'http_code': 200,
            }
            if not auth:
                response["status"] = "error"
                response["message"] = "Missing authorization"
                response["http_code"] = 400
                handler.set_status(response["http_code"])
                handler._transforms = []
                handler.write(response["message"])
                handler.finish()
                return
            parts = auth.split()
            handler._transforms = []
            nparts = len(parts)
            auth_type = parts[0].lower()
            if auth_type != 'bearer' or nparts==1 or nparts>2:
                handler.set_status(400)
                response["status"] = "error"
                response["message"] = "Invalid header authorization"
                handler.write(response["message"])
                handler.finish()
                return
            token = parts[1]
            try:
                response = validate_token(token)
                if response["status"] != STATUS_OK:
                    handler.set_status(response['http_code'])
                    handler.write(response["message"])
                    handler.finish()
                    return
            except Exception as e:
                handler.set_status(500)
                handler.write(e.message)
                handler.finish()
                return
            ## Include a decoded token in the request handler instance for use by the @allowed_roles decorator
            handler._token_decoded = response["decoded_token"]
            # # If the token was valid, issue a new token to extend the session
            # new_token = encode_info(user_info=decoded_token)
            # decoded_new_token = jwt.decode(new_token, config['jwt']['hs256Secret'], algorithms=["HS256"])
            # handler._token_decoded = decoded_new_token
            # handler._token_encoded = new_token.decode(encoding='UTF-8')
        def _execute(self, transforms, *args, **kwargs):
            check_auth(self, kwargs)
            return handler_execute(self, transforms, *args, **kwargs)
        return _execute
    cls_handler._execute = wrap_execute(cls_handler._execute)
    return cls_handler


# The @allowed_roles decorator must be accompanied by a preceding @authenticated decorator, allowing
# it to restrict access to the decorated function to authenticated users with specified roles.
def allowed_roles(roles_allowed=[]):
    # Always allow admin access
    roles_allowed.append('api_admin')
    # Actual decorator is check_roles
    def check_roles(cls_handler):
        def wrap_execute(handler_execute):
            def wrapper(handler, kwargs):
                response = {
                    "status": STATUS_OK,
                    "msg": ""
                }
                try:
                    roles = handler._token_decoded[config['oidc']['groups']['keyName']]
                    ## Keycloak groups start with a leading forward slash. Assume Keycloak sub-groups
                    ## are not used and that the `roles_allowed` list consists of simple words 
                    ## like "admin" or "collaborator".
                    parsed_roles = []
                    for role in roles:
                        parsed_roles.append(role.strip('/'))
                    roles = parsed_roles
                    if not any(role in roles for role in roles_allowed):
                        handler.set_status(403)
                        response["status"] = STATUS_ERROR
                        response["message"] = "Access denied. You are not in the authorized group or role."
                        handler.write(response["message"])
                        handler.finish()
                        return
                except:
                    handler.set_status(500)
                    response["status"] = STATUS_ERROR
                    response["message"] = "Error authorizing access."
                    return
            def _execute(self, transforms, *args, **kwargs):
                wrapper(self, kwargs)
                return handler_execute(self, transforms, *args, **kwargs)
            return _execute
        cls_handler._execute = wrap_execute(cls_handler._execute)
        return cls_handler
    return check_roles
