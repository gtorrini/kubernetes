import requests
from global_vars import config

import logging

# Configure logging
logging.basicConfig(
    format='%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s',
)
log = logging.getLogger("spt-api")
# handler = logging.StreamHandler()
# formatter = logging.Formatter('%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')
# handler.setFormatter(formatter)
# log.addHandler(handler)
try:
    log.setLevel(config['server']['logLevel'].upper())
except:
    log.setLevel('WARNING')

class KeycloakApi():
    ## ref: https://www.keycloak.org/docs-api/15.0/rest-api/#_users_resource
    def __init__(self):
        self.auth_token = ''
        self.refresh_token = ''
        self.admin_username = config['keycloak']['admin']['username']
        self.admin_password = config['keycloak']['admin']['password']
        self.base_url = config['keycloak']['base_url']
        self.login()

    def login(self):
        # Login to obtain an auth token
        r = requests.request( 'POST',
            config['keycloak']['login_url'],
            data={
                'client_id': 'admin-cli',
                'grant_type': 'password',
                'username': self.admin_username,
                'password': self.admin_password,
            },
        )
        # Store the JWT auth token
        try:
            assert r.status_code == 200
            response = r.json()
            # log.debug(json.dumps(response, indent=2))
            self.auth_token = response['access_token']
            self.refresh_token = response['refresh_token']
        except:
            log.error(f'''Error authenticating user "{self.admin_username}". Exiting.''')
            log.error(r.status_code)
            log.error(r.text)

    def get_user_info(self, email=''):
        log.debug(f'''{self.base_url}/users''')
        response = requests.request( 'GET',
            f'''{self.base_url}/users''',
            params={
                'email': email,
                # 'exact': True,
                'enabled': True,
            },
            headers={'Authorization': 'Bearer {}'.format(self.auth_token)},
        )
        try:
            response = response.json()
        except:
            log.error(response.text)
        log.debug(response)
        return response

    def apply_role_mapping_to_user(self, user_id, role_id='', role_name='', realm='spt3g'):
        assert role_id and role_name and realm
        params = [{
            "id": role_id,
            "name": role_name,
            "description": "{"+role_name+"}",
            "composite": False,
            "clientRole": False,
            "containerId": realm,
        }]

        r = requests.request( 'POST',
            f'''{self.base_url}/users/{user_id}/role-mappings/realm''',
            json=params,
            headers={'Authorization': 'Bearer {}'.format(self.auth_token)},
        )
        try:
            assert r.status_code in [200, 204]
            success = True
        except:
            success = False
            log.error(f'''Error adding "{user_id}" to role "{role_name}".''')
            log.error(r.status_code)
            log.error(r.text)
        return success

    def get_user_role_mapping(self, user_id, realm='spt3g'):
        assert user_id and realm
        response = requests.request( 'GET',
            f'''{self.base_url}/users/{user_id}/role-mappings''',
            headers={'Authorization': 'Bearer {}'.format(self.auth_token)},
        )
        try:
            assert response.status_code in [200, 204]
            response = response.json()
        except:
            log.error(f'''Error getting "{user_id}" realm role mappings.''')
            log.error(f'''[{response.status_code}] {response.text}''')
        return response

    def get_user_group_membership(self, user_id, realm='spt3g'):
        assert user_id and realm
        response = requests.request( 'GET',
            f'''{self.base_url}/users/{user_id}/groups''',
            headers={'Authorization': 'Bearer {}'.format(self.auth_token)},
        )
        try:
            assert response.status_code in [200, 204]
            response = response.json()
        except:
            log.error(f'''Error getting "{user_id}" group memberships.''')
            log.error(f'''[{response.status_code}] {response.text}''')
        return response

    def add_user_to_group(self, user_id, group_name=''):
        assert user_id and group_name
        
        response = requests.request( 'GET',
            f'''{self.base_url}/groups/''',
            headers={'Authorization': 'Bearer {}'.format(self.auth_token)},
        )
        try:
            assert response.status_code in [200, 204]
            groups = response.json()
        except:
            log.error(f'''Error getting group list.''')
            log.error(f'''[{response.status_code}] {response.text}''')
        log.debug(f'''groups: {groups}''')
        group_id = [group['id'] for group in groups if group['name'] == group_name]
        if not group_id:
            log.error(f'''Error getting group id.''')
            return None
        group_id = group_id[0]
        response = requests.request( 'PUT',
            f'''{self.base_url}/users/{user_id}/groups/{group_id}''',
            headers={'Authorization': 'Bearer {}'.format(self.auth_token)},
        )
        try:
            assert response.status_code in [200, 204]
        except:
            log.error(f'''Error getting "{user_id}" group memberships.''')
            log.error(f'''[{response.status_code}] {response.text}''')
        return response
