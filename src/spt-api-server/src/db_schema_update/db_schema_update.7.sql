#---
CREATE TABLE IF NOT EXISTS `stats_jobs`(
	`id` int NOT NULL AUTO_INCREMENT,
	`time_scanned` datetime NOT NULL,
	`num_total` integer NOT NULL,
	`num_complete` integer NOT NULL,
	`num_error` integer NOT NULL,
	`num_aborted` integer NOT NULL,
	`avg_duration` integer NOT NULL,
	`num_users` integer NOT NULL,
	`dist_users` text NOT NULL,
	`dist_user_agents` text NOT NULL,
	PRIMARY KEY (`id`), UNIQUE KEY `id` (`id`)
)
