#!/usr/bin/env python

import requests
from requests.auth import HTTPBasicAuth
import xml.etree.ElementTree as ET
import os
import json
from global_vars import config
import logging

# Configure logging
logging.basicConfig(
    format='%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s',
)
log = logging.getLogger("spt-api")
try:
    log.setLevel(config['server']['logLevel'].upper())
except:
    log.setLevel('WARNING')


class NextcloudApi():
    ## ref: https://docs.nextcloud.com/server/latest/admin_manual/
    def __init__(self, base_url='', auth_token='', auth_user=''):
        self.auth_token = auth_token
        self.auth_user = auth_user
        self.base_url = base_url
        assert self.base_url and self.auth_token
        self.api_base_url = f'''{self.base_url.strip('/')}/ocs/v2.php/cloud'''

    def get_users(self):
      return self.get_common_list(list_type='users')

    def get_groups(self):
      return self.get_common_list(list_type='groups')

    def get_common_list(self, list_type=''):
        assert list_type
        response = requests.request('GET',
            f'''{self.api_base_url}/{list_type}''',
            headers={'Authorization': 'Bearer {}'.format(self.auth_token)},
        )
        try:
            assert response.status_code in [200, 204]
            root = ET.fromstring(response.text)
            user_list = [item.text for item in root.findall(
                f'./data/{list_type}/element')]
            response = user_list
        except:
            log.error(f'''[{response.status_code}] {response.text}''')
            response = None
        return response

    def get_user_info(self, user_id=''):
        assert user_id
        response = requests.request('GET',
            f'''{self.api_base_url}/users/{user_id}''',
            headers={'Authorization': 'Bearer {}'.format(self.auth_token)},
        )
        try:
            assert response.status_code in [200, 204]
            root = ET.fromstring(response.text)
            user_info = {}
            for key in root.findall('./data/*'):
                # print(key.findall())
                if key.tag == 'groups':
                    user_info['groups'] = [
                        item.text for item in root.findall(f'./data/groups/element')]
                else:
                    try:
                        user_info[key.tag] = key.text.strip()
                    except:
                        user_info[key.tag] = key.text
            response = user_info
        except:
            log.error(f'''[{response.status_code}] {response.text}''')
            response = None
        return response

    def get_group_members(self, group=''):
        assert group
        response = requests.request('GET',
            f'''{self.api_base_url}/groups/{group}''',
            headers={'Authorization': 'Bearer {}'.format(self.auth_token)},
        )
        try:
            assert response.status_code in [200, 204]
            root = ET.fromstring(response.text)
            user_list = [item.text for item in root.findall(f'./data/users/element')]
            response = user_list
        except:
            log.error(f'''[{response.status_code}] {response.text}''')
            response = None
        return response


class NextcloudDeckApi():
    ## ref: https://deck.readthedocs.io/en/latest/API/
    def __init__(self, base_url='', auth_token='', auth_user=''):
        self.auth_token = auth_token
        self.auth_user = auth_user
        self.base_url = base_url
        assert self.base_url and self.auth_token
        self.api_base_url = f'''{self.base_url.strip('/')}/apps/deck/api/v1.0'''

    def get_deck_boards(self):
        response = requests.request('GET',
            f'''{self.api_base_url}/boards''',
            headers={
                'OCS-APIRequest': 'true',
                'Content-Type': 'application/json',
            },
            auth = HTTPBasicAuth(self.auth_user, self.auth_token),
        )
        try:
            assert response.status_code in [200, 204]
            response = response.json()
        except:
            log.error(f'''[{response.status_code}] {response.text}''')
            response = None
        return response

    def get_deck_stacks(self, board_id=''):
        assert board_id
        response = requests.request('GET',
            f'''{self.api_base_url}/boards/{board_id}/stacks''',
            headers={
                'OCS-APIRequest': 'true',
                'Content-Type': 'application/json',
            },
            auth = HTTPBasicAuth(self.auth_user, self.auth_token),
        )
        try:
            assert response.status_code in [200, 204]
            response = response.json()
        except:
            log.error(f'''[{response.status_code}] {response.text}''')
            response = None
        return response


    def get_deck_card(self, board_id='', stack_id='', card_id=''):
        assert board_id and stack_id and card_id
        response = requests.request('GET',
            f'''{self.api_base_url}/boards/{board_id}/stacks/{stack_id}/cards/{card_id}''',
            headers={
                'OCS-APIRequest': 'true',
                'Content-Type': 'application/json',
            },
            auth = HTTPBasicAuth(self.auth_user, self.auth_token),
        )
        try:
            assert response.status_code in [200, 204]
            response = response.json()
        except:
            log.error(f'''[{response.status_code}] {response.text}''')
            response = None
        return response


    def deck_create_card(self, board_id='', stack_id='', card_title='', card_description=''):
        assert board_id and stack_id and card_title
        response = requests.request('POST',
            f'''{self.api_base_url}/boards/{board_id}/stacks/{stack_id}/cards''',
            headers={
                'OCS-APIRequest': 'true',
                'Content-Type': 'application/json',
            },
            auth = HTTPBasicAuth(self.auth_user, self.auth_token),
            json = {
                'title': card_title, # 	String 	The title of the card, maximum length is limited to 255 characters
                'type': 'plain', # 	String 	Type of the card (for later use) use 'plain' for now
                'order': 999, # 	Integer 	Order for sorting the stacks
                'description': card_description, # 	String 	(optional) The markdown description of the card
                # 'duedate': , # 	timestamp 	(optional) The duedate of the card or null
            }
        )
        try:
            assert response.status_code in [200, 204]
            log.debug(response.text)
            response = response.json()
        except:
            log.error(f'''[{response.status_code}] {response.text}''')
            response = None
        return response


    def deck_delete_card(self, board_id='', stack_id='', card_id=''):
        assert board_id and stack_id and card_id
        response = requests.request('DELETE',
            f'''{self.api_base_url}/boards/{board_id}/stacks/{stack_id}/cards/{card_id}''',
            headers={
                'OCS-APIRequest': 'true',
                'Content-Type': 'application/json',
            },
            auth = HTTPBasicAuth(self.auth_user, self.auth_token),
        )
        try:
            assert response.status_code in [200, 204]
            response = True
        except:
            log.error(f'''[{response.status_code}] {response.text}''')
            response = False
        return response



    def deck_card_assign_user(self, board_id='', stack_id='', card_id='', user_id=''):
        assert board_id and stack_id and card_id and user_id
        response = requests.request('PUT',
            f'''{self.api_base_url}/boards/{board_id}/stacks/{stack_id}/cards/{card_id}/assignUser''',
            headers={
                'OCS-APIRequest': 'true',
                'Content-Type': 'application/json',
            },
            auth = HTTPBasicAuth(self.auth_user, self.auth_token),
            json = {
                'userId': user_id,
            }
        )
        try:
            assert response.status_code in [200, 204]
            response = True
        except:
            log.error(f'''[{response.status_code}] {response.text}''')
            response = False
        return response


def main():
    import argparse
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description='''
    Nextcloud API library. 
    Set the environment variables like so:
        export NC_BASE_URL='https://cloud.example.com'
        export NC_AUTH_USER='user-1234' 
        export NC_AUTH_TOKEN='abcde-fghij-klmno-pqrst-uvwxy' 
    where the NC_AUTH_TOKEN is an app password generated by https://cloud.example.com/settings/user/security 
    with NC_AUTH_USER being the associated username.
    ''')
    args = parser.parse_args()

    api = NextcloudApi(
        base_url=os.environ['NC_BASE_URL'],
        auth_token=os.environ['NC_AUTH_TOKEN'],
        auth_user=os.environ['NC_AUTH_USER'],
    )


if __name__ == "__main__":
    main()
