import sys
import requests
import logging
import yaml
import os
import time

# Configure logging
logging.basicConfig(
    format='%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s',
)
log = logging.getLogger("spt-api")
try:
    log.setLevel(config['server']['logLevel'].upper())
except:
    log.setLevel('DEBUG')

# Configure authentication and endpoints
# TO DO: Figure out how to change these values at helm chart level
#        and where to securely store API token
os.environ['SPT_API_TOKEN'] = 'monitor-botApiToken'
CONFIG = {
    'auth_token': os.environ['SPT_API_TOKEN'],
    'apiBaseUrl': 'https://spt3g.ncsa.illinois.edu/api/v1',
    'filesBaseUrl': 'https://spt3g.ncsa.illinois.edu/files/jobs',
    }

def submit_cutout_job(data):
    log.info('Submitting cutout job...')
    job_info = {}
    response = requests.request('PUT', 
        f'''{CONFIG['apiBaseUrl']}/uws/job''', 
        headers={'Authorization': f'''Bearer {CONFIG['auth_token']}'''},
        json=data)
    try:
        assert response.status_code in [200, 204]
        job_info = response.json()
    except:
        log.error(f'''[{response.status_code}] {response.text}''')
        return job_info
    return job_info


def get_job_status(job_id: str = '') -> list:
    """Get status of individual job or all jobs belonging to the authenticated user."""
    job_info = {}
    # Validate inputs
    assert isinstance(job_id, str) or not job_id
    url = f'''{CONFIG['apiBaseUrl']}/uws/job'''
    if job_id:
        url += f'''/{job_id}'''
    # Fetch job status
    response = requests.request('GET',
        url,
        headers={'Authorization': f'''Bearer {CONFIG['auth_token']}'''})
    try:
        assert response.status_code in [200, 204]
        job_info = response.json()
    except:
        log.error(f'''[{response.status_code}] {response.text}''')
        return job_info
    return job_info


def job_status_poll(job_id):
    log.info(f'Polling status of job "{job_id}"...')
    job_info = {}
    while not job_info or ('phase' in job_info and job_info['phase'] in ['pending', 'queued', 'executing']):
        # Fetch the current job status
        job_info = get_job_status(job_id)
        # print(json.dumps(job_info))
        time.sleep(3)
    print('\n')
    return job_info


def post_signal():
    log.info('Posting signal file...')
    url = 'insert/api/endpoint/here'
    post_info = {}
    with open('path/to/signal/file.ext', 'rb') as f:
        response = requests.request('POST',
            url,
            headers={'Authorization': f'''Bearer {CONFIG['auth_token']}'''},
            files={'signal_file.ext': f})
    try:
        assert response.status_code in [200, 204]
        post_info = response.json()
    except:
        log.error(f'''[{response.status_code}] {response.text}''')
        return post_info
    return post_info


def delete_signal():
    log.info('Deleting signal file...')
    url = 'insert/api/endpoint/here'
    delete_info = {}
    response = requests.request('DELETE',
        url,
        headers={'Authorization': f'''Bearer {CONFIG['auth_token']}'''})
    try:
        assert response.status_code in [200, 204]
        delete_info = response.json()
    except:
        log.error(f'''[{response.status_code}] {response.text}''')
        return delete_info
    return delete_info


def run_monitor():
    # Load configuration for test job
    with open('testjob_config.yml', 'r') as config_file:
        job_config = yaml.load(config_file, Loader=yaml.FullLoader)

    # Submit test job
    job_info = submit_cutout_job(job_config)
    if not job_info:
        return
    job_id = job_info['jobId']
    log.info('Job ' + str(job_id) + ' submitted.')

    # Poll until completion
    job_status = job_status_poll(job_id)
    if not job_status:
        return
    phase = job_status['phase']
    if phase != 'completed':
        log.error(f'''Job did not complete successfully: {phase}''')
        return
    log.info('Job ' + str(job_id) +' completed.')

    # # PENDING FURTHER DISCUSSION:
    # # Post signal file
    # posted = post_signal()
    # if not posted:
    #     return
    # log.info('Signal file posted.')

    # # What happens in between right here?

    # # Delete signal file
    # deleted = delete_signal()
    # if not delete_info:
    #     return
    # log.info('Signal file deleted.')
    return


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Execute periodic functions required by the API server.')
    parser.add_argument('--monitor', action='store_true', help='Run monitor job.')
    args = parser.parse_args()

    if args.monitor:
        log.info('Running monitor...')
        run_monitor()
