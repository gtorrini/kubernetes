{{ if .Values.cronjob.enabled }}
---
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: {{ .Release.Name }}-cron-status
spec:
  schedule: {{ .Values.cronjob.schedule1 | quote }}
  concurrencyPolicy: Replace
  jobTemplate:
    spec:
      ttlSecondsAfterFinished: {{ .Values.cronjob.ttlSecondsAfterFinished }}
      backoffLimit: {{ .Values.cronjob.backoffLimit }}
      template:
        metadata:
          labels:
            type: {{ .Release.Name }}-cron-status
        spec:
          restartPolicy: "OnFailure"
          {{- with .Values.deployment.securityContext }}
          securityContext:
            runAsUser: {{ .runAsUser }}
            runAsGroup: {{ .runAsGroup }}
            fsGroup: {{ .fsGroup }}
          {{- end }}
          serviceAccountName: {{ .Release.Name }}-foreman
          {{- with .Values.deployment.image.pullSecrets }}
          imagePullSecrets:
            {{ toYaml . | indent 6 }}
          {{- end }}
          volumes:
          - name: config
            configMap:
              name: {{ .Release.Name }}-config
          - name: {{ .Values.config.uws.workingVolume.name }}
            persistentVolumeClaim:
              claimName: {{ .Values.config.uws.workingVolume.claimName }}
          {{- with .Values.extraConfigFromSecret }}
          - name: secret-config
            secret:
              secretName: {{ . }}
          {{- end }}
          containers:
          - name: cron
            image: {{ .Values.deployment.image.repository }}:{{ .Values.deployment.image.tag }}
            imagePullPolicy: {{ .Values.deployment.image.pullPolicy }}
            command: ["python3", "cron.py", "--status", "--email"]
            volumeMounts:
            - name: config
              mountPath: /etc/config/server.yaml
              subPath: server.yaml
            - name: {{ .Values.config.uws.workingVolume.name }}
              mountPath: {{ .Values.config.uws.workingVolume.mountPath }}
              subPath: {{ .Values.config.uws.workingVolume.subPath }}
            {{- if .Values.extraConfigFromSecret }}
            - name: secret-config
              mountPath: /etc/config/secret.yaml
              subPath: secret.yaml
            {{- end }}
            env:
            - name: MARIADB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: spt3g-api-db
                  key: mariadb-password
            - name: MARIADB_USERNAME
              valueFrom:
                secretKeyRef:
                  name: spt3g-api-db
                  key: username
            - name: MARIADB_DATABASE
              valueFrom:
                secretKeyRef:
                  name: spt3g-api-db
                  key: database
            - name: MARIADB_HOST
              value: "spt3g-api-db"
            {{- with .Values.deployment.environment }}
            {{- range . }}
            - name: "{{ .name }}"
              value: "{{ .value }}"
            {{- end }}
            {{- end }}
---
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: {{ .Release.Name }}-cron-garbage
spec:
  schedule: {{ .Values.cronjob.schedule2 | quote }}
  concurrencyPolicy: Replace
  jobTemplate:
    spec:
      ttlSecondsAfterFinished: {{ .Values.cronjob.ttlSecondsAfterFinished }}
      backoffLimit: {{ .Values.cronjob.backoffLimit }}
      template:
        metadata:
          labels:
            type: {{ .Release.Name }}-cron-garbage
        spec:
          restartPolicy: "OnFailure"
          {{- with .Values.deployment.securityContext }}
          securityContext:
            runAsUser: {{ .runAsUser }}
            runAsGroup: {{ .runAsGroup }}
            fsGroup: {{ .fsGroup }}
          {{- end }}
          serviceAccountName: {{ .Release.Name }}-foreman
          {{- with .Values.deployment.image.pullSecrets }}
          imagePullSecrets:
            {{ toYaml . | indent 6 }}
          {{- end }}
          volumes:
          - name: config
            configMap:
              name: {{ .Release.Name }}-config
          - name: {{ .Values.config.uws.workingVolume.name }}
            persistentVolumeClaim:
              claimName: {{ .Values.config.uws.workingVolume.claimName }}
          {{- with .Values.extraConfigFromSecret }}
          - name: secret-config
            secret:
              secretName: {{ . }}
          {{- end }}
          containers:
          - name: cron
            image: {{ .Values.deployment.image.repository }}:{{ .Values.deployment.image.tag }}
            imagePullPolicy: {{ .Values.deployment.image.pullPolicy }}
            command: ["python3", "cron.py", "--garbage"]
            volumeMounts:
            - name: config
              mountPath: /etc/config/server.yaml
              subPath: server.yaml
            - name: {{ .Values.config.uws.workingVolume.name }}
              mountPath: {{ .Values.config.uws.workingVolume.mountPath }}
              subPath: {{ .Values.config.uws.workingVolume.subPath }}
            {{- if .Values.extraConfigFromSecret }}
            - name: secret-config
              mountPath: /etc/config/secret.yaml
              subPath: secret.yaml
            {{- end }}
            env:
            - name: MARIADB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: spt3g-api-db
                  key: mariadb-password
            - name: MARIADB_USERNAME
              valueFrom:
                secretKeyRef:
                  name: spt3g-api-db
                  key: username
            - name: MARIADB_DATABASE
              valueFrom:
                secretKeyRef:
                  name: spt3g-api-db
                  key: database
            - name: MARIADB_HOST
              value: "spt3g-api-db"
            {{- with .Values.deployment.environment }}
            {{- range . }}
            - name: "{{ .name }}"
              value: "{{ .value }}"
            {{- end }}
            {{- end }}
---
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: {{ .Release.Name }}-cron-expire-jobs
spec:
  schedule: {{ .Values.cronjob.schedule3 | quote }}
  concurrencyPolicy: Replace
  jobTemplate:
    spec:
      ttlSecondsAfterFinished: {{ .Values.cronjob.ttlSecondsAfterFinished }}
      backoffLimit: {{ .Values.cronjob.backoffLimit }}
      template:
        metadata:
          labels:
            type: {{ .Release.Name }}-cron-expire-jobs
        spec:
          restartPolicy: "OnFailure"
          {{- with .Values.deployment.securityContext }}
          securityContext:
            runAsUser: {{ .runAsUser }}
            runAsGroup: {{ .runAsGroup }}
            fsGroup: {{ .fsGroup }}
          {{- end }}
          serviceAccountName: {{ .Release.Name }}-foreman
          {{- with .Values.deployment.image.pullSecrets }}
          imagePullSecrets:
            {{ toYaml . | indent 6 }}
          {{- end }}
          volumes:
          - name: config
            configMap:
              name: {{ .Release.Name }}-config
          - name: {{ .Values.config.uws.workingVolume.name }}
            persistentVolumeClaim:
              claimName: {{ .Values.config.uws.workingVolume.claimName }}
          {{- with .Values.extraConfigFromSecret }}
          - name: secret-config
            secret:
              secretName: {{ . }}
          {{- end }}
          containers:
          - name: cron
            image: {{ .Values.deployment.image.repository }}:{{ .Values.deployment.image.tag }}
            imagePullPolicy: {{ .Values.deployment.image.pullPolicy }}
            command: ["python3", "cron.py", "--expire"]
            volumeMounts:
            - name: config
              mountPath: /etc/config/server.yaml
              subPath: server.yaml
            - name: {{ .Values.config.uws.workingVolume.name }}
              mountPath: {{ .Values.config.uws.workingVolume.mountPath }}
              subPath: {{ .Values.config.uws.workingVolume.subPath }}
            {{- if .Values.extraConfigFromSecret }}
            - name: secret-config
              mountPath: /etc/config/secret.yaml
              subPath: secret.yaml
            {{- end }}
            env:
            - name: MARIADB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: spt3g-api-db
                  key: mariadb-password
            - name: MARIADB_USERNAME
              valueFrom:
                secretKeyRef:
                  name: spt3g-api-db
                  key: username
            - name: MARIADB_DATABASE
              valueFrom:
                secretKeyRef:
                  name: spt3g-api-db
                  key: database
            - name: MARIADB_HOST
              value: "spt3g-api-db"
            {{- with .Values.deployment.environment }}
            {{- range . }}
            - name: "{{ .name }}"
              value: "{{ .value }}"
            {{- end }}
            {{- end }}

---
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: {{ .Release.Name }}-cron-update-stats
spec:
  schedule: {{ .Values.cronjob.schedule4 | quote }}
  concurrencyPolicy: Replace
  jobTemplate:
    spec:
      ttlSecondsAfterFinished: {{ .Values.cronjob.ttlSecondsAfterFinished }}
      backoffLimit: {{ .Values.cronjob.backoffLimit }}
      template:
        metadata:
          labels:
            type: {{ .Release.Name }}-cron-update-stats
        spec:
          restartPolicy: "OnFailure"
          {{- with .Values.deployment.securityContext }}
          securityContext:
            runAsUser: {{ .runAsUser }}
            runAsGroup: {{ .runAsGroup }}
            fsGroup: {{ .fsGroup }}
          {{- end }}
          serviceAccountName: {{ .Release.Name }}-foreman
          {{- with .Values.deployment.image.pullSecrets }}
          imagePullSecrets:
            {{ toYaml . | indent 6 }}
          {{- end }}
          volumes:
          - name: config
            configMap:
              name: {{ .Release.Name }}-config
          - name: {{ .Values.config.uws.workingVolume.name }}
            persistentVolumeClaim:
              claimName: {{ .Values.config.uws.workingVolume.claimName }}
          {{- with .Values.extraConfigFromSecret }}
          - name: secret-config
            secret:
              secretName: {{ . }}
          {{- end }}
          containers:
          - name: cron
            image: {{ .Values.deployment.image.repository }}:{{ .Values.deployment.image.tag }}
            imagePullPolicy: {{ .Values.deployment.image.pullPolicy }}
            command: ["python3", "stats.py", "--all"]
            volumeMounts:
            - name: config
              mountPath: /etc/config/server.yaml
              subPath: server.yaml
            - name: {{ .Values.config.uws.workingVolume.name }}
              mountPath: {{ .Values.config.uws.workingVolume.mountPath }}
              subPath: {{ .Values.config.uws.workingVolume.subPath }}
            {{- if .Values.extraConfigFromSecret }}
            - name: secret-config
              mountPath: /etc/config/secret.yaml
              subPath: secret.yaml
            {{- end }}
            env:
            - name: MARIADB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: spt3g-api-db
                  key: mariadb-password
            - name: MARIADB_USERNAME
              valueFrom:
                secretKeyRef:
                  name: spt3g-api-db
                  key: username
            - name: MARIADB_DATABASE
              valueFrom:
                secretKeyRef:
                  name: spt3g-api-db
                  key: database
            - name: MARIADB_HOST
              value: "spt3g-api-db"
            {{- with .Values.deployment.environment }}
            {{- range . }}
            - name: "{{ .name }}"
              value: "{{ .value }}"
            {{- end }}
            {{- end }}
{{- end }}
