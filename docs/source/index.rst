SPT-3G Data Services
====================

The `National Center for Supercomputing Applications (NCSA) <https://www.ncsa.illinois.edu>`_ at the University of Illinois develops and operates data services for the South Pole Telescope - 3rd Generation (SPT-3G) project.

This documentation includes resources for end users of the data services as well as information for system administrators and app developers. At NCSA, we are innovating both at the scientific application level -- such as the cutout service -- and at the level of the research platform, where our Kubernetes-based deployment provides reproducibility, scalability, and the *flexibility* modern scientific enterprise demands.

.. toctree::
   :maxdepth: 2
   :caption: User Docs
   :glob:

   user/*

.. toctree::
   :maxdepth: 1
   :caption: Admin and Developer Docs
   :glob:

   admin/*
