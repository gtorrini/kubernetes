Administration and Access
=============================

The administrators of the Kubernetes cluster are [Felipe Menanteau](mailto:felipe@illinois.edu) and [Andrew Manning](mailto:manninga@illinois.edu). Access to deployments and secrets originates from

- access to the Kubernetes cluster controlplane nodes host OS via SSH, and
- membership in the SPT3G (`spt3g`) GitLab group.

The kubeconfig that provides admin access to Kubernetes resources is provided by the [NCSA Rancher instance](https://gonzo-rancher.ncsa.illinois.edu) that is configured to manage the cluster. This provides `kubectl` access to all cluster resources.

GitLab authentication is used to provide access to [the ArgoCD instance](https://argocd.spt3g.ncsa.illinois.edu) and the deployment repo that together drive the deployment of services.
