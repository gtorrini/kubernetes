
Documentation System
====================

Overview
-----------

The documentation system uses Sphinx to render source documents (formatted in Markdown and RestructuredText) as an interactive website. This HTML output is copied into a Docker container based on the NGINX webserver image, which is deployed via Kubernetes to power the project documentation website.

Development environment
-------------------------

Build the documentation and run a local webserver using Docker:

```bash
$ docker build -t registry.gitlab.com/spt3g/kubernetes/docs:latest .
$ docker run --rm -it \
    -p 8080:80 \
    -v $(pwd)/source/api:/usr/share/nginx/html/docs/api \
    registry.gitlab.com/spt3g/kubernetes/docs:latest
```

The volume flag `-v` mounts the local `api` directory to the container so that you can edit the `openapi.yaml` file and see the changes immediately at http://127.0.0.1:8080/docs/api/ upon reloading the page without rebuilding the image.

Initialization
-----------------

```bash
$ sphinx-quickstart 
Welcome to the Sphinx 1.7.9 quickstart utility.

Please enter values for the following settings (just press Enter to
accept a default value, if one is given in brackets).

Selected root path: .

You have two options for placing the build directory for Sphinx output.
Either, you use a directory "_build" within the root path, or you separate
"source" and "build" directories within the root path.
> Separate source and build directories (y/n) [n]: y

Inside the root directory, two more directories will be created; "_templates"
for custom HTML templates and "_static" for custom stylesheets and other static
files. You can enter another prefix (such as ".") to replace the underscore.
> Name prefix for templates and static dir [_]: 

The project name will occur in several places in the built documentation.
> Project name: SPT3G
> Author name(s): T. Andrew Manning, Felipe Menanteau
> Project release []: 

If the documents are to be written in a language other than English,
you can select a language here by its language code. Sphinx will then
translate text that it generates into that language.

For a list of supported codes, see
http://sphinx-doc.org/config.html#confval-language.
> Project language [en]: 

The file name suffix for source files. Commonly, this is either ".txt"
or ".rst".  Only files with this suffix are considered documents.
> Source file suffix [.rst]: 

One document is special in that it is considered the top node of the
"contents tree", that is, it is the root of the hierarchical structure
of the documents. Normally, this is "index", but if your "index"
document is a custom template, you can also set this to another filename.
> Name of your master document (without suffix) [index]: 

Sphinx can also add configuration for epub output:
> Do you want to use the epub builder (y/n) [n]: 
Indicate which of the following Sphinx extensions should be enabled:
> autodoc: automatically insert docstrings from modules (y/n) [n]: 
> doctest: automatically test code snippets in doctest blocks (y/n) [n]: 
> intersphinx: link between Sphinx documentation of different projects (y/n) [n]: 
> todo: write "todo" entries that can be shown or hidden on build (y/n) [n]: 
> coverage: checks for documentation coverage (y/n) [n]: 
> imgmath: include math, rendered as PNG or SVG images (y/n) [n]: 
> mathjax: include math, rendered in the browser by MathJax (y/n) [n]: 
> ifconfig: conditional inclusion of content based on config values (y/n) [n]: 
> viewcode: include links to the source code of documented Python objects (y/n) [n]: 
> githubpages: create .nojekyll file to publish the document on GitHub pages (y/n) [n]: 

A Makefile and a Windows command file can be generated for you so that you
only have to run e.g. `make html' instead of invoking sphinx-build
directly.
> Create Makefile? (y/n) [y]: 
> Create Windows command file? (y/n) [y]: n

Creating file ./source/conf.py.
Creating file ./source/index.rst.
Creating file ./Makefile.

Finished: An initial directory structure has been created.

You should now populate your master file ./source/index.rst and create other documentation
source files. Use the Makefile to build the docs, like so:
   make builder
where "builder" is one of the supported builders, e.g. html, latex or linkcheck.
```

Automated build using GitLab CI/CD pipeline
----------------------------------------------

Add an image build workflow for the Sphinx-based docs by adding the following to `gitlab-ci.yaml`, which will use the `docs/Dockerfile` to run the Sphinx build when the contents of `docs/` change on the `main` branch:

```yaml
build-sphinx-docs:
  stage: build
  only:
    refs:
    - main
    changes:
    - docs/**/*
  image: docker:19.03.12
  services:
  - docker:19.03.12-dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker pull $CI_REGISTRY_IMAGE/docs:latest || true
    - >
      if [[ "$CI_COMMIT_REF_NAME" == "main" ]]; then
        BUILDTAG="latest"
      else
        BUILDTAG="$CI_COMMIT_REF_NAME"
      fi
    - >
      docker build
      --cache-from $CI_REGISTRY_IMAGE/docs:latest
      --tag $CI_REGISTRY_IMAGE/docs:$CI_COMMIT_SHA
      --tag $CI_REGISTRY_IMAGE/docs:$BUILDTAG
      docs/
    - docker push $CI_REGISTRY_IMAGE/docs:$CI_COMMIT_SHA
    - docker push $CI_REGISTRY_IMAGE/docs:$BUILDTAG
```
