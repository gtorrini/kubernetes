import os
import sys
import requests
import time
import yaml

try:
    assert os.environ['SPT_API_TOKEN']
except:
    print(f'''Export an environment variable SPT_API_TOKEN containing your API token prior to executing this script:
    export SPT_API_TOKEN="eyJ0eXAiO...f72LVU"
    python {sys.argv[0]}
    ''')
    sys.exit(1)


CONFIG = {
    'auth_token': os.environ['SPT_API_TOKEN'],
    'apiBaseUrl': 'https://spt3g.ncsa.illinois.edu/api/v1',
    'filesBaseUrl': 'https://spt3g.ncsa.illinois.edu/files/jobs',
}


def submit_cutout_job(
    positions: str = '',
    date_start: str = '',
    date_end: str = '',
    bands: list = None,
    filetypes: list = None,
    yearly_coadd: list = None,
    email: bool = False,
) -> dict:
    job_info = {}
    """Submits a cutout job and returns the complete server response which includes the job ID."""
    ## Validate inputs
    assert positions and date_start and date_end
    data = {
        'positions': positions,
        'date_start': date_start,
        'date_end': date_end,
        'email': email,
    }
    if bands:
        data['bands'] = bands
    if filetypes:
        data['filetypes'] = filetypes
    if yearly_coadd:
        data['yearly_coadd'] = yearly_coadd
    ## Submit job
    response = requests.request('PUT',
        f'''{CONFIG['apiBaseUrl']}/uws/job''',
        headers={'Authorization': f'''Bearer {CONFIG['auth_token']}'''},
        json=data,
    )
    try:
        assert response.status_code in [200, 204]
        job_info = response.json()
    except:
        print(f'''[{response.status_code}] {response.text}''')
        return job_info
    return job_info


def get_job_status(job_id: str = '') -> list:
    """Get status of individual job or all jobs belonging to the authenticated user."""
    job_info = {}
    ## Validate inputs
    assert isinstance(job_id, str) or not job_id
    url = f'''{CONFIG['apiBaseUrl']}/uws/job'''
    if job_id:
        url += f'''/{job_id}'''
    ## Fetch job status
    response = requests.request('GET',
        url,
        headers={'Authorization': f'''Bearer {CONFIG['auth_token']}'''},
    )
    try:
        assert response.status_code in [200, 204]
        job_info = response.json()
    except:
        print(f'''[{response.status_code}] {response.text}''')
        return job_info
    return job_info


def delete_job(job_id: str = '') -> list:
    """Delete individual job"""
    ## Validate inputs
    assert isinstance(job_id, str) and job_id
    ## Delete job
    response = requests.request('DELETE',
        f'''{CONFIG['apiBaseUrl']}/uws/job/{job_id}''',
        headers={'Authorization': f'''Bearer {CONFIG['auth_token']}'''},
    )
    try:
        assert response.status_code in [200, 204]
        return True
    except:
        print(f'''[{response.status_code}] {response.text}''')
        return False


def job_status_poll(job_id):
    print(f'Polling status of job "{job_id}"...', end='')
    job_info = {}
    while not job_info or ('phase' in job_info and job_info['phase'] in ['pending', 'queued', 'executing']):
        print('.', end='', sep='', flush=True)
        # Fetch the current job status
        job_info = get_job_status(job_id)
        # print(json.dumps(job_info))
        time.sleep(3)
    print('\n')
    return job_info


def download_job_files(job_id=None, base_url=None, download_dir=None):
    if not job_id:
        return
    if not base_url:
        base_url = f'''{CONFIG["filesBaseUrl"]}/{job_id}/out'''
    if not download_dir:
        download_base_dir = os.path.abspath(os.getcwd())
        download_sub_dir = os.path.join('cutout_job_files', job_id)
        download_dir = os.path.join(download_base_dir, download_sub_dir)
        print(f'''Downloading job files to "{download_sub_dir}"...''')
    os.makedirs(download_dir, exist_ok=True)
    response = requests.request('GET', f'''{base_url}/json''')
    for item in response.json():
        suburl = f'''{base_url}/{item['name']}'''
        if item['type'] == 'directory':
            subdir = f'''{download_dir}/{item['name']}'''
            download_job_files(job_id=job_id, base_url=suburl, download_dir=subdir)
        elif item['type'] == 'file':
            try:
                data = requests.request('GET', suburl, stream=True)
                with open(f'''{download_dir}/{item['name']}''', "wb") as file:
                    for chunk in data.iter_content(chunk_size=8192):
                        file.write(chunk)
            except Exception as e:
                print(f'''Error fetching result file "suburl": {e}''')


def main():
    ## Load job configuration
    ##
    with open('job_config.yaml', "r") as conf_file:
        job_config = yaml.load(conf_file, Loader=yaml.FullLoader)
    # print(yaml.dump(job_config, indent=2))
    
    ## Submit new cutout job
    ##
    print(f'Submitting new cutout job...')
    job_info = submit_cutout_job(
        positions=job_config['positions'],
        date_start=job_config['date_start'],
        date_end=job_config['date_end'],
        yearly_coadd=job_config['yearly_coadd'] if 'yearly_coadd' in job_config else None,
        bands=job_config['bands'] if 'bands' in job_config else None,
        email=job_config['email'] if 'email' in job_config else False,
    )
    if not job_info:
        return
    job_id = job_info['jobId']

    ## Poll the status of the job
    ##
    job_status = job_status_poll(job_id)
    if not job_status:
        return
    phase = job_status['phase']
    if phase != 'completed':
        print(f'''Job did not complete successfully: "{phase}"''')
        return

    ## Download the job output files
    ##
    download_job_files(job_id=job_id)
    print(f'Done.')

if __name__ == "__main__":
    main()
